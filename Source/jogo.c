#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <ctype.h>
#include "jogo.h"
#include "utils.h"

#define LeftMargin 5 //margem direita para impress�o do tabuleiros

// L� jogo_peca do tabuleiro
int jogo_peca(pjogo jogo, int linha, int coluna) {
	return *(jogo->tabuleiro + coluna + linha * jogo->colunas);
}

/* imprime o tabuleiro da forma
 A  B  C  D
 1  -- -- --
 |  |  |  |
 2  -- -- --
 |  |  |  |
 3  -- -- --
 */
void jogo_mostra_tabuleiro(pjogo jogo) {
	char ch;
	int i, l, c;
	// printHeader("Tabuleiro");
	printStrings(" ", LeftMargin);
	// imprime letras
	for (i = 65; i < jogo->colunas + 65; i++) {
		printf("%c  ", (char)i);
	}
	printf("\n");

	for (l = 0; l < jogo->linhas; l++) {
		printf("%5d", l + 1);
		for (c = 0; c < jogo->colunas; c++) {
			if (jogo_peca(jogo, l, c) != 0) {
				ch = 'X';
			}
			else {
				ch = ' ';
			};
			printf("%c", ch);
			if (c < jogo->colunas - 1)
				printf("--");
		}
		printf("\n");
		if (l < jogo->linhas - 1) {
			printStrings(" ", LeftMargin);
			printStrings("|  ", jogo->colunas);
			printf("\n");
		}
	}
	printf("\n");
}

void jogo_mostra(pjogo jogo) {
	int i, j, l, c;
	printHeader("Jogo do Quadrado");
	printf("Jogadas: %d; Ultimas jogadas:", jogo->nJogadas);
	// mostra ultimas jogadas
	j = jogo->nJogadas - jogo->nJogadores;
	if (j < 0)
		j = 0;
	for (i = j; i < jogo->nJogadas; i++) {
		l = jogo->pecasJogadas[i] / jogo->colunas + 1;
		c = jogo->pecasJogadas[i] % jogo->colunas;
		printf("%c%d;", c + 'A', l);
	}
	printf("\n");
	jogo_mostra_tabuleiro(jogo);
}

int jogo_aumenta_tabuleiro(pjogo jogo) {
	int *NovoTab;
	int *pecasJogadas;
	int l, c, i;
	int linhasAntigas=jogo->linhas;
	int colunasAntigas=jogo->colunas;
	int aumentou = 0;
	if (jogo->linhas < TAB_NLINHAS_JOGO_MAX) {
		jogo->linhas++;
		aumentou = 1;
	}
	if (jogo->colunas < TAB_NCOLUNAS_JOGO_MAX) {
		jogo->colunas++;
		aumentou = 1;
	}

	if (aumentou == 0)
		return 0;

	NovoTab = (int*)malloc(sizeof(int) * jogo->colunas * jogo->linhas);
	if (NovoTab == NULL) {
		return 1;
	}
	for (l = 0; l < jogo->linhas; l++) {
		for (c = 0; c < jogo->colunas; c++) {
			if (c >= (colunasAntigas) || l >= (linhasAntigas)) {
				*(NovoTab + c + l * jogo->colunas) = 0;
			}
			else {
				*(NovoTab + c + l * jogo->colunas) = *(jogo->tabuleiro + c + l * (colunasAntigas));
			}

		}
	}
	// converte pe�as indice de pe�as jogadas
	pecasJogadas = realloc(jogo->pecasJogadas, sizeof(int) * jogo->linhas * jogo->colunas);
	if (pecasJogadas == NULL) {
		return 1;
	}
	jogo->pecasJogadas = pecasJogadas;
	for (i = 0; i < jogo->nJogadas; i++) {
		l = *(jogo->pecasJogadas + i) / (colunasAntigas);
		c = *(jogo->pecasJogadas + i) % (colunasAntigas);
		*(jogo->pecasJogadas + i) = c + l * jogo->colunas;
	}
	free(jogo->tabuleiro);
	jogo->tabuleiro = NovoTab;
	return 0;
}

// Inicia variaveis do jogo
void jogo_inicia_variaveis(pjogo jogo) {
	jogo->tabuleiro = NULL;
	jogo->pecasJogadas = NULL;
}

// Liberta a mem�ria alocada no jogo
void jogo_finaliza(pjogo jogo) {
	jogo->linhas=0;
	jogo->colunas=0;
	jogo->nJogadas=0;
	if (jogo->tabuleiro != NULL)
		free(jogo->tabuleiro);
	jogo->tabuleiro = NULL;
	if (jogo->pecasJogadas != NULL)
		free(jogo->pecasJogadas);
	jogo->pecasJogadas = NULL;
}

// reinicia tabuleiro
void jogo_reinica_tabuleiro(pjogo jogo, int linhas, int colunas) {
	int i;
	if (jogo->tabuleiro != NULL)
		free(jogo->tabuleiro);
	if (jogo->pecasJogadas != NULL)
		free(jogo->pecasJogadas);

	jogo->linhas = linhas;
	jogo->colunas = colunas;

	jogo->tabuleiro = malloc(sizeof(int) * jogo->linhas * jogo->colunas);
	jogo->pecasJogadas = malloc(sizeof(int) * jogo->linhas * jogo->colunas);

	jogo->nJogadas = 0;
	for (i = 0; i < jogo->linhas * jogo->colunas; i++) {
		*(jogo->tabuleiro + i) = 0;
		*(jogo->pecasJogadas) = -1;
	}
}

//pesquisa o nome do jogador no jogo
int jogo_pesquisa_jogador(pjogo jogo, char *nome) {
	int i;
	for(i=0;i<jogo->nJogadores;i++)
	{
		if (stricmp(nome,jogo->jogadores[i].nome)==0)
		{
		   return 1;
		}
	}
	return 0;
}

// Inicia as vari�veis do jogo
void jogo_inicia(pjogo jogo, int linhas, int colunas) {
	jogo_finaliza(jogo);
	jogo->linhas = linhas;
	jogo->colunas = colunas;
	jogo->nJogadores = 0;
	jogo->nJogador = 0;
	jogo->nJogadorAnterior = -1;
	jogo->nJogadoresEmJogo = jogo->nJogadores;
	jogo->ultimoStat = ret_proxima_jogada;
	gerar_palavrapasse(jogo->palavra_passe,PALAVA_PASSE_TAM);
	jogo_reinica_tabuleiro(jogo, linhas, colunas);
}

void jogo_adiciona_jogadorAnonimo(pjogo jogo, char *nome) {
	strcpy(jogo->jogadores[jogo->nJogadores].nome, nome);
	jogo->jogadores[jogo->nJogadores].id = -jogo->nJogadores-1; // os jogadores anonimos s�o negativos
	jogo->jogadores[jogo->nJogadores].perdeu = 0;
	jogo->jogadores[jogo->nJogadores].undo = 0;
	jogo->jogadores[jogo->nJogadores].jogador = NULL;
	jogo->nJogadores++;
	jogo->nJogadoresEmJogo++;
}

void jogo_adiciona_jogador(pjogo jogo, pjogadorlist jogador) {
	strcpy(jogo->jogadores[jogo->nJogadores].nome, jogador->nome);
	jogo->jogadores[jogo->nJogadores].id = jogador->id;
	jogo->jogadores[jogo->nJogadores].perdeu = 0;
	jogo->jogadores[jogo->nJogadores].undo = 0;
	jogo->jogadores[jogo->nJogadores].jogador = jogador;
	jogo->nJogadores++;
	jogo->nJogadoresEmJogo++;
}

void jogo_remove_jogador(pjogo jogo, int id) {
	int i;
	if (id > jogo->nJogadores)
		return;
	for (i = id - 1; i < jogo->nJogadores + 1; i++) {
		jogo->jogadores[i] = jogo->jogadores[i + 1];
	}
	jogo->nJogadores--;
}

// converte coordenadas do tipo "A1" para indice do array
int jogo_coor2index(pjogo jogo, char *op) {
	int i;
	int linha = 0;
	int coluna = 0;

	for (i = 0; i < strlen(op); i++) {
		if (isalpha(op[i]) && coluna == 0) {
			coluna = op[i] - 'A';
		}
		else if (isdigit(op[i])) {
			if (linha != 0) {
				linha = linha * 10;
			}
			linha += (op[i] - '0');
		}
		else {
			return -1;
		}
	}
	if (coluna < 0 || coluna >= jogo->colunas) {
		return -1;
	}
	if (linha <= 0 || linha > jogo->linhas) {
		return -1;
	}
	linha--;
	return coluna + (linha*jogo->colunas);
}

// Verifica se a jogada faz um quadrado
int jogo_verifica_jogada(pjogo jogo, int idx) {
	int linha_ini = idx / jogo->colunas; // posicao primeira linha
	int coluna_ini = idx % jogo->colunas; // posicao primeira coluna
	int tam_aresta = 0; // tamanho da aresta a testar...
	int c;

	// Lado esquerdo
	for (c = coluna_ini - 1; c >= 0; c--) {
		if (jogo_peca(jogo, linha_ini, c) != 0) {
			// encontrou uma jogada na linha, testar com este tamanho de aresta
			tam_aresta = (coluna_ini - c);
			// testar se cabe para cima
			if (linha_ini - tam_aresta >= 0) {
				// validar quadrado
				if ((jogo_peca(jogo, linha_ini - tam_aresta, coluna_ini - tam_aresta) != 0) && (jogo_peca(jogo,
					linha_ini - tam_aresta, coluna_ini) != 0)) {
					return 1;
				}
			}
			// testar se cabe para baixo
			if (coluna_ini - tam_aresta >= 0 && linha_ini + tam_aresta < jogo->linhas) {
				// validar quadrado
				if ((jogo_peca(jogo, linha_ini + tam_aresta, coluna_ini - tam_aresta) != 0) && (jogo_peca(jogo,
					linha_ini + tam_aresta, coluna_ini) != 0)) {
					return 1;
				}
			}
		}
	}

	// Lado direito
	for (c = coluna_ini + 1; c < jogo->colunas; c++) {
		if (jogo_peca(jogo, linha_ini, c) != 0) {
			// encontrou uma jogada na linha, testar com este tamanho de aresta
			tam_aresta = (c - coluna_ini);
			// testar se cabe para cima
			if (linha_ini - tam_aresta >= 0) {
				// validar quadrado
				if ((jogo_peca(jogo, linha_ini - tam_aresta, coluna_ini + tam_aresta) != 0) && (jogo_peca(jogo,
					linha_ini - tam_aresta, coluna_ini) != 0)) {
					return 1;
				}
			}
			// testar se cabe para baixo
			if (jogo->linhas - linha_ini >= tam_aresta) {
				if ((jogo_peca(jogo, linha_ini + tam_aresta, tam_aresta - coluna_ini) != 0) && (jogo_peca(jogo,
					linha_ini + tam_aresta, coluna_ini) != 0)) {
					return 1;
				}
			}
		}
	}

	return 0;
}

enum jogo_ret jogo_undo(pjogo jogo) {
	int i;

	if (jogo->jogadores[jogo->nJogador].undo == 0) {
		for (i = jogo->nJogadas - 1; i >= 0 && i > jogo->nJogadas - jogo->nJogadores - 1; i--) {
			*(jogo->tabuleiro + *(jogo->pecasJogadas + i)) = 0;
		}
		jogo->nJogadas = jogo->nJogadas - jogo->nJogadores;
		if (jogo->nJogadas < 0) {
			jogo->nJogadas = 0;
		}
		jogo->jogadores[jogo->nJogador].undo = 1;
		return ret_continua;
	}
	else {
		return ret_ja_fez_undo;
	}
}

// Regista a jogada no tabuleiro
void jogo_regista_jogada(pjogo jogo, int idx) {
	*(jogo->pecasJogadas + jogo->nJogadas) = idx;
	jogo->nJogadas++;
	// regista jogada
	*(jogo->tabuleiro + idx) = jogo->jogadores[jogo->nJogador].id;
}

void jogo_continua(pjogo jogo, pjogadores_ctrl jctrl) {
	enum jogo_ret ret;
	do {
		jogo_mostra(jogo);
		ret = jogo_menu(jogo, jctrl);
	}
	while ((ret != ret_ganhou) && (ret != ret_menu) && (ret != ret_erro_memoria));

	if (ret == ret_erro_memoria) {
		printf("Erro de alocacao de memoria!");
		WaitEnterKey(1);
	}
}

/*Jogo tem jogadors anomimos?
 0 - n�o tem
 1 - tem
*/
int jogo_tem_anonimos(pjogo jogo){
   int i;
   for(i=0;i<jogo->nJogadores;i++) 
   {
	   if (jogo->jogadores[i].id<0) return 1;
   
   }
   return 0;
}

void jogo_gravar(pjogo jogo, char *nomeficheiro)
{
  FILE *f;
  int i;
  f=fopen(nomeficheiro,"wb");
  if (f==NULL) {
	  printf("Erro a abrir o ficheiro para gravar\n");
	  WaitEnterKey(1);
	  return;
  }
  fwrite(JOGO_HEADER,(int)JOGO_HEADER_SIZE,1,f);
  fwrite(jogo->palavra_passe,PALAVA_PASSE_TAM,1,f);
  fwrite(&jogo->linhas,sizeof(int),1,f);
  fwrite(&jogo->colunas,sizeof(int),1,f);
  fwrite(&jogo->nJogador,sizeof(int),1,f);
  fwrite(&jogo->ultimoStat,sizeof(enum jogo_ret),1,f);
  fwrite(&jogo->nJogadas,sizeof(int),1,f);
  fwrite(&jogo->nJogadoresEmJogo,sizeof(int),1,f);
  fwrite(&jogo->nJogadores,sizeof(int),1,f);
  fwrite(&jogo->nJogadorAnterior,sizeof(int),1,f);
  fwrite(jogo->tabuleiro,sizeof(int),jogo->linhas*jogo->colunas,f);
  fwrite(jogo->pecasJogadas,sizeof(int),jogo->nJogadas,f);
  for (i = 0; i < jogo->nJogadores; i++) {
	 fwrite(&jogo->jogadores[i].id,sizeof(int),1,f);
	 fwrite(&jogo->jogadores[i].perdeu,sizeof(int),1,f);
     fwrite(&jogo->jogadores[i].undo,sizeof(int),1,f);
  }


  fclose(f);
}

//L� jogo do disco
//devolve 0 se jogo inv�lido 1-se jogo ok
int jogo_ler(pjogadores_ctrl jctrl, pjogo jogo, char *nomeficheiro, char *palavrapasse)
{
  FILE *f;
  int i;
  char header[JOGO_HEADER_SIZE];
  pjogadorlist jogador=NULL;
  f=fopen(nomeficheiro,"rb");
  if (f==NULL) {
	  printf("Erro a abrir o ficheiro para ler.\n");
	  WaitEnterKey(1);
	  return 0;
  }
  fread(header,(int)JOGO_HEADER_SIZE,1,f);
  if (strcmp(header,JOGO_HEADER)!=0)
  {
      printf("Ficheiro %s nao e um ficheiro de jogo valido.\n",nomeficheiro);
	  WaitEnterKey(1);
	  fclose(f);
	  return 0;
  }
  jogo_finaliza(jogo);
  jogo_inicia(jogo,0,0);
  
  fread(jogo->palavra_passe,PALAVA_PASSE_TAM,1,f);
  if (strcmp(palavrapasse,jogo->palavra_passe)!=0)
  {
      printf("Palavra-passe incorrecta.\n");
	  WaitEnterKey(1);
	  fclose(f);
	  return 0;
  }


  fread(&jogo->linhas,sizeof(int),1,f);
  fread(&jogo->colunas,sizeof(int),1,f);
  jogo_reinica_tabuleiro(jogo,jogo->linhas,jogo->colunas);
  fread(&jogo->nJogador,sizeof(int),1,f);
  fread(&jogo->ultimoStat,sizeof(enum jogo_ret),1,f);
  fread(&jogo->nJogadas,sizeof(int),1,f);
  fread(&jogo->nJogadoresEmJogo,sizeof(int),1,f);
  fread(&jogo->nJogadores,sizeof(int),1,f);
  fread(&jogo->nJogadorAnterior,sizeof(int),1,f);
  fread(jogo->tabuleiro,sizeof(int),jogo->linhas*jogo->colunas,f);
  fread(jogo->pecasJogadas,sizeof(int),jogo->nJogadas,f);
  for (i = 0; i < jogo->nJogadores; i++) {
	 fread(&jogo->jogadores[i].id,sizeof(int),1,f);
	 fread(&jogo->jogadores[i].perdeu,sizeof(int),1,f);
     fread(&jogo->jogadores[i].undo,sizeof(int),1,f);
	 jogador=jogadores_encontra(jctrl,jogo->jogadores[i].id);
	 if (jogador==NULL) {
	     printf("Jogo invalidado ja nao existe o jogador %d\n",jogo->jogadores[i].id);
		 WaitEnterKey(1);
		 fclose(f);
		 return 0;
	 } else
	 {
		 strcpy(jogo->jogadores[i].nome,jogador->nome);
	 }
  }  
  fclose(f);
  return 1;
}


// Controlo do jogada
enum jogo_ret jogo_main(pjogo jogo, char *aop) {
	int idx;
	char op[10];
	char nomeficheiro[NOME_FICHEIRO_TAM];

	strcpy(op, aop);
	strupr(op); // Converte para Maisculas para compara��o
	if (strcmp(op, "G") == 0) {
		// Grava o jogo
		if (jogo_tem_anonimos(jogo))
		{
			printf("Nao pode gravar jogos com jogadores anonimos!\n");
			WaitEnterKey(1);			
		} else
		{
			readString("Nome do ficheiro (vazio para cancelar):",nomeficheiro,NOME_FICHEIRO_TAM,0);
			if (strlen(nomeficheiro)>0) {
			   jogo_gravar(jogo,nomeficheiro);
			   printf("Para retomar o jogo mais tarde tem de ter esta palavra-passe: %s\n",jogo->palavra_passe);
			   WaitEnterKey(1);
			}
			}
	}
	else if (strcmp(op, "M") == 0) {
		// volta para o menu
		return ret_menu;
	}
	else if (strcmp(op, "U") == 0) {
		return jogo_undo(jogo);
	}
	else {
		idx = jogo_coor2index(jogo, op);
		if (idx == -1) {
			return ret_jogada_invalida;
		}
		if (*(jogo->tabuleiro + idx) != 0) {
			return ret_ja_jogado;
		}
		// regista jogada
		jogo_regista_jogada(jogo, idx);
		// Valida jogada
		if (jogo_verifica_jogada(jogo, idx)) {
			jogo->jogadores[jogo->nJogador].perdeu = jogo->nJogadoresEmJogo;
			jogo->nJogadoresEmJogo--;
			if (jogo->nJogadoresEmJogo > 1) {
				if (jogo_aumenta_tabuleiro(jogo) == 1) {
					return ret_erro_memoria;
				}
			}
            jogo_proximo_jogador(jogo);			
			return ret_perdeu;
		}
		jogo_proximo_jogador(jogo);
		return ret_continua;
	}
	return ret_continua;
}

// Proximo jogador...
void jogo_proximo_jogador(pjogo jogo) {
	int i;
	// proximo jogador que ainda n�o perdeu

	for (i = jogo->nJogador + 1; i < jogo->nJogadores; i++) {
		if (jogo->jogadores[i].perdeu == 0) {
			jogo->nJogadorAnterior = jogo->nJogador;
			jogo->nJogador = i;
			return;
		}
	}
	for (i = 0; i < jogo->nJogador; i++) {
		if (jogo->jogadores[i].perdeu == 0) {
			jogo->nJogadorAnterior = jogo->nJogador;
			jogo->nJogador = i;
			return;
		}
	}
}

// Menu do jogo
enum jogo_ret jogo_menu(pjogo jogo, pjogadores_ctrl jctrl) {
	char op[5];

	switch (jogo->ultimoStat) {
	case ret_perdeu:
		if (jogo->jogadores[jogo->nJogadorAnterior].id>0) 
		{
			jogo->jogadores[jogo->nJogadorAnterior].jogador->j_perdidos++;
		}
		printf("O jogador %s perdeu.\n", jogo->jogadores[jogo->nJogadorAnterior].nome);
		if (jogo->nJogadoresEmJogo == 1) {
			if (jogo->jogadores[jogo->nJogador].id>0) 
		    {
				jogo->jogadores[jogo->nJogador].jogador->j_ganhos++;
				jogadores_top_reordena(jctrl);
		    }
			printf("O jogador %s ganhou! Parabens.\n", jogo->jogadores[jogo->nJogador].nome);
			WaitEnterKey(1);
			jogo_finaliza(jogo);
			jogo->ultimoStat = ret_ganhou;
			return jogo->ultimoStat;
		}
		break;
	case ret_jogada_invalida:
		printf("Jogada invalida\n");
		break;
	case ret_ja_fez_undo:
		printf("Ja fez undo uma vez, nao pode voltar a fazer...\n");
		break;
	case ret_ja_jogado:
		printf("Preenchida, escolha outra!\n");
		break;
	default: ;
	}
	printf("(M)enu, (U)ndo, (G)ravar, %s joga:", jogo->jogadores[jogo->nJogador].nome);
	readString("", op, 5, 0);
	jogo->ultimoStat = jogo_main(jogo, op);
	return jogo->ultimoStat;
}
