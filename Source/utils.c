#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <time.h>
#include "utils.h"

#define Linhas 79

void clrscr() {
	HANDLE h_stdout;
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	DWORD dwConSize;
	COORD coordScreen = {0, 0};
	DWORD dwCharsWritten;

	h_stdout = GetStdHandle(STD_OUTPUT_HANDLE);

	GetConsoleScreenBufferInfo(h_stdout, &csbi);
	dwConSize = csbi.dwSize.X * csbi.dwSize.Y;

	FillConsoleOutputCharacter(h_stdout, (TCHAR)' ', dwConSize, coordScreen, &dwCharsWritten);

	SetConsoleCursorPosition(h_stdout, coordScreen);

}

// imprime a string str nvezes
void printStrings(char *str, int nvezes) {
	int i;
	for (i = 0; i < nvezes; i++)
		printf("%s", str);
}

// imprime a string no centro
void printCenter(char *str, int tam) {
	int left = (tam - strlen(str)) / 2;
	printStrings(" ", left);
	printf("%s", str);
	printStrings(" ", tam - left - strlen(str));
}

// imprime a string � esquerda
void print_left(char *str, int tam, int pontos) {
	int i;

	for (i = 0; i < strlen(str) && i < tam; i++) {
		if ((pontos == 1) && (i >= tam - 3) && (strlen(str) > tam)) {
			printf(".");
		}
		else {
			printf("%c", str[i]);
		}
	}
	for (; i < tam; i++) {
		printf(" ");
	}
}

// imprime a string � direira
void print_right(char *str, int tam, int pontos) {
	int indx = 0;
	int i;
	printStrings(" ", tam - strlen(str));
	if (strlen(str) > tam) {
		indx = strlen(str) - tam;
		if (pontos) {
			printf("...");
			indx = indx + 3;
		}

	}
	for (i = indx; str[i] != '\0'; i++) {
		printf("%c", str[i]);
	}
}

void print_lefti(int value, int tam, int pontos) {
	char v[10];
	itoa(value, v, 10);
	print_left(v, tam, pontos);
}

void print_righti(int value, int tam, int pontos) {
	char v[10];
	itoa(value, v, 10);
	print_right(v, tam, pontos);
}

// imprime a string na forma de cabe�alho
void printHeader(char *str) {
	clrscr();
	printStrings("=", Linhas - 1);
	printf("\n=");
	printCenter(str, Linhas - 3);
	printf("=\n");
	printStrings("=", Linhas - 1);
	printf("\n");
}

int readInteger(char *msg, int min, int max) {
	int op;
	char opstr[5];
	do {
		if (max == 0) {
			printf("\n%s", msg);
		}
		else {
			printf("\n%s (%d-%d):", msg, min, max);
		}
		readString("", opstr, 5, 0);
		op = atoi(opstr);
	}
	while ((op < min) || (op > max) && max != 0);
	return op;
}

void readString(char *msg, char *str, int maxlen, int vazio) {
	char ch;
	int i = 0; // posicao
	printf(msg);
	*str = '\0';
	do {
		ch = getchar();
		if (ch == '\n') {
			if (vazio == 0 || i > 0)
			{
				break;
			}
			continue;
		}
		if (i < maxlen) {
			str[i] = ch;
			if (i+1<maxlen) i++;
			
		}
	}
	while (1);
	if (maxlen>1 && i<maxlen) 
	{ 
	   str[i] = '\0';
	}
}

// L� uma op��o do teclado
// input
// msg: a Mensagem a apresentar na consola
// opcoes: array de char com os caracteres possiveis.
// output
// devolve char escolhido
// exemplo:
// char ch=readOpcao("Escolha uma opcao:","AB12346")
char readOpcao(char *msg, char *opcoes) {
	char ch;
	int i;
	do {
		printf("\n%s", msg);
		readString("", &ch, 1, 0);
		ch = toupper(ch);
		for (i = 0; i < strlen(opcoes); i++) {
			if (ch == opcoes[i])
				return opcoes[i];
		}
		printf("opcao errada!");
	}
	while (1);

}

void WaitEnterKey(int ShowText) {
	char ch;
	if (ShowText == 1) {
		printf("Precione a tecla 'enter' para continuar...");
	}
	fflush(stdin);
	while (ch = (getchar() != '\n'));

}

void gotoxy(int x, int y) {
	HANDLE h_stdout;
	COORD coordScreen;

	h_stdout = GetStdHandle(STD_OUTPUT_HANDLE);

	coordScreen.X = x;
	coordScreen.Y = y;

	SetConsoleCursorPosition(h_stdout, coordScreen);

}

int searchStr(char *str, char *search) {

	int i = 0, j = 0, k = 0, l = 0, k1 = 0;
	l = strlen(search);

	while (str[i] != '\0' && k == 0) {
		if (toupper(str[i]) == toupper(search[j])) {
			i++;
			j++;
			k1 = 1;

			if (j == l) {
				j = 0;
				k = 1;
			}
		}
		else {
			if (k1 == 1) {
				j = 0;
				k1 = 0;
			}
			else
				i++;
		}
	}

	return k;
}

void gerar_palavrapasse(char *palavrapasse, int tam) {
	int contador;
	int indice;
	char caracter;
	char consoantes[] = "bcdfghjklmnoprstvwxz";
	char vogais[] = "aeiouy";

	for (contador = 0; contador < tam; ++contador) {
		if (contador % 2 == 0) {
			indice = rand() % 6;
			caracter = vogais[indice];
		}
		else {
			indice = rand() % 20;
			caracter = consoantes[indice];
		}
		palavrapasse[contador] = caracter;
	}
	palavrapasse[tam-1] = '\0';
}
