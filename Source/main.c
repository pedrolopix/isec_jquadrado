#include <stdio.h>
#include <string.h>
#include "utils.h"
#include "jogo.h"

int menu_inicial(jogadores_ctrl jogadores) {
    int i;
	printHeader("Jogo do Quadrado");
	printf("Programacao - ISEC 2011 - Pedro Lopes 21200565 e Jorge Carvalho 21190512\n\n");
	printf("Estatistica:\t  N. jogadores registados: %d\n",jogadores.nJogadores);
	printf("\t\t  N. jogos: %d\n",jogadores.nJogos);

	if (jogadores.top[0]!=NULL) {
		printf("\nTOP 10",jogadores.nJogadores);
		for(i=0;i<JOGODORES_TOP && jogadores.top[i]!=NULL;i++)
		{
			printf("\t\t %2d. %s \t %d\n",i+1,jogadores.top[i]->nome,jogadores.top[i]->j_ganhos);
		}
	}
	printf("\nMENU");
	printf("\t1 - Novo Jogo\n");
	printf("\t2 - Retomar Jogo\n");
	printf("\t3 - Jogadores\n");
	printf("\t4 - Ler jogo\n");
	printf("\t5 - Sair\n");
	printf("\n");

	return readOpcao("Escolha uma opcao:", "123456T");
}

void mostra_jogadores_escolhidos(pjogo jogo, pjogadores_ctrl jogadores) {
	if (jogo->nJogadores > 0) {
		printf("Jogadores escolhidos");
		jogo->jogadores;
	}

}

void novo_jogo(pjogo jogo, pjogadores_ctrl jogadores) {
	int i;
	char nome[NOME_JOGADOR_TAM];
	char opcao;
	pjogadorlist jogador;
	jogo_inicia(jogo, TAB_NLINHAS_DEF, TAB_NCOLUNAS_DEF);
	do {
		printHeader("Novo Jogo");
		printf("\n");
		printf("  Numero de linhas: %d\n", jogo->linhas);
		printf("  Numero de colunas: %d\n", jogo->colunas);
		printf("  Numero de jogadores: %d\n", jogo->nJogadores);
		printf("\n");
		if (jogo->nJogadores > 0) {
			printf("Jogadores:\n");
			for (i = 0; i < jogo->nJogadores; i++) {
				printf("\t%d. %s\n", i + 1, jogo->jogadores[i].nome);
			}
		}

		opcao = readOpcao("(M)enu, (A)nomimo, (J)ogador, (R)emover, (C)olunas, (L)inhas, (I)niciar:", "MAJRCLI");
		switch (opcao) {
		case 'M':
			jogo_finaliza(jogo); // limpa informação do jogo
			return;
			break;
		case 'A':
			if (jogo->nJogadores>=NJOGADORES_MAX) {
				printf("O jogo apenas permite %d jogadores em simultaneo\n",NJOGADORES_MAX);
				WaitEnterKey(1);	
				break;
			}
			readString("Nome do jogador:", nome, NOME_JOGADOR_TAM, 0);
			if (strlen(nome) > 0) {
				if (jogo_pesquisa_jogador(jogo,nome)==1)
				{
					printf("Ja existe um jogador com o nome %s no jogo...!\n",nome);
					WaitEnterKey(1);
				} else  
				{
					jogo_adiciona_jogadorAnonimo(jogo, nome);

				}
			}
			break;
		case 'J':
		    if (jogo->nJogadores>=NJOGADORES_MAX) {
				printf("O jogo apenas permite %d jogadores em simultaneo\n",NJOGADORES_MAX);
				WaitEnterKey(1);	
				break;
			}
			jogador = jogadores_main(jogadores, 1);
			if (jogo_pesquisa_jogador(jogo,jogador->nome)==1)
			{
				printf("Ja existe um jogador com o nome %s no jogo...!\n",jogador->nome);
				WaitEnterKey(1);
			} else if (jogador != NULL) {
				jogo_adiciona_jogador(jogo, jogador);
			}
			break;
		case 'R':
			i = readInteger("Jogador para remover (0-cancela):", 0, jogo->nJogadores);
			if (i > 0) {
				jogo_remove_jogador(jogo, i);
			}
			break;
		case 'C':
			i = readInteger("Numero de colunas:", TAB_NCOLUNAS_MIN, TAB_NCOLUNAS_MAX);
			jogo_reinica_tabuleiro(jogo, jogo->linhas, i);
			break;
		case 'L':
			i = readInteger("Numero de linhas:", TAB_NLINHAS_MIN, TAB_NLINHAS_MAX);
			jogo_reinica_tabuleiro(jogo, i, jogo->colunas);
			break;
		case 'I':
			if (jogo->nJogadores < 2) {
				printf("Sao precisos 2 jogadores para iniciar o jogo!\n");
				WaitEnterKey(1);
			}
			break;
		}
	}
	while (opcao != 'I' || (jogo->nJogadores < 2));
	jogo_continua(jogo,jogadores);
}

void main_ler_jogo(pjogo jogo, pjogadores_ctrl jogadores)
{
	char nome[NOME_FICHEIRO_TAM]=""; 
	char password[PALAVA_PASSE_TAM]="";
	readString("Nome do ficheiro:",nome,NOME_FICHEIRO_TAM,0);
	if (strlen(nome)==0) return;
	readString("Palavra-passe:",password,PALAVA_PASSE_TAM,1);

	if (jogo_ler(jogadores,jogo, nome, password)) 
	{
		jogo_continua(jogo,jogadores);
	}
}

int main(int argc, char *argv[]) {

	int op;
	jogo jogo;
	jogadores_ctrl jogadores;
	jogo_inicia_variaveis(&jogo);
	jogadores_inicia_variaveis(&jogadores);
	jogadores_le(&jogadores, NOME_FICHEIRO_JOGADORES);


	do {
		op = menu_inicial(jogadores);

		switch (op) {
		case '1':
			novo_jogo(&jogo, &jogadores);
			break;
		case '2':
			if (jogo.tabuleiro==NULL)
			{
				printf("O jogo ainda nao foi inicializado!\n");
				WaitEnterKey(1);
			} else
			{
			  jogo_continua(&jogo, &jogadores);
			}
			break;
		case '3':
			jogadores_main(&jogadores, 0);
			break;
		case '4':
			main_ler_jogo(&jogo,&jogadores);
			break;
		}
	}
	while (op != '5');
	jogadores_grava(&jogadores, NOME_FICHEIRO_JOGADORES);
	jogo_finaliza(&jogo);
	jogadores_finaliza_variaveis(&jogadores);

	return 0;
}
