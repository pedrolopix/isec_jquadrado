#define NOME_JOGADOR_TAM  20
#define NOME_FICHEIRO_JOGADORES "JOGADORES.DAT"
#define JOGODORES_TOP 10
/* Estrutura dos Jogadores */
typedef struct jogador_struct jogadorlist, *pjogadorlist;
typedef struct jogadores_struct jogadores_ctrl, *pjogadores_ctrl;

struct jogador_struct {
	char nome[NOME_JOGADOR_TAM];
	int id;
	int j_ganhos;
	int j_perdidos;
	pjogadorlist prox; /* Ponteiro para o proximo da lista */
};

struct jogadores_struct {
	pjogadorlist list;
	int nJogadores;
	int nJogos;
	int lastid;
	pjogadorlist top[JOGODORES_TOP];
};

pjogadorlist jogadores_encontra(pjogadores_ctrl lista, int id);
pjogadorlist jogadores_escolhe(pjogadores_ctrl lista);
pjogadorlist jogadores_main(pjogadores_ctrl lista, int encontra);
void jogadores_finaliza_variaveis(pjogadores_ctrl jogadores);
void jogadores_grava(pjogadores_ctrl jogadores, char *nomeficheiro);
void jogadores_inicia_variaveis(pjogadores_ctrl jogadores);
void jogadores_le(pjogadores_ctrl jogadores, char *nomeficheiro);
void jogadores_top(pjogadores_ctrl ctrl, pjogadorlist jogador);
void jogadores_top_reordena(pjogadores_ctrl ctrl);
void jogadores_visualizar(pjogadores_ctrl jctrl, char *filtro, int Pagina, int tamPagina);
