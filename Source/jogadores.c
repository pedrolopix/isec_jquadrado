#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "jogadores.h"
#include "utils.h"

void jogadores_inicia_variaveis(pjogadores_ctrl jogadores) {
	int i;
	jogadores->list = NULL;
	jogadores->nJogadores = 0;
	jogadores->nJogos = 0;
	jogadores->lastid = 0;
	for (i = 0; i < JOGODORES_TOP; i++)
		jogadores->top[i] = NULL;
}

void jogadores_finaliza_variaveis(pjogadores_ctrl jogadores) {
	pjogadorlist p, q;
	int i;
	// desliga top de jogadores
	for (i = 0; i < JOGODORES_TOP; i++)
		jogadores->top[i] = NULL;

	// liberta lista de jogadores
	if (jogadores->list != NULL) {
		p = jogadores->list;
		while (p != NULL) {
			q = p;
			p = p->prox;
			free(q);
		}
		jogadores->list = NULL;
	}
}

void jogadores_grava(pjogadores_ctrl jogadores, char *nomeficheiro) {
	FILE *f;
	pjogadorlist list;
	f = fopen(nomeficheiro, "wb");
	if (f == NULL) {
		printf("Erro a criar o ficheiro %s", nomeficheiro);
	}
	else {
		// gravar jogadores
		list = jogadores->list;
		while (list != NULL) {
			if (fwrite(list, sizeof(jogadorlist), 1, f) != 1) {
				printf("Erro a gravar o ficheiro %s", nomeficheiro);
				fclose(f);
				return;
			}
			list = list->prox;
		}
		fclose(f);
	}
}

void jogadores_le(pjogadores_ctrl jogadores, char *nomeficheiro) {
	FILE *f;
	pjogadorlist novo, ultimo = NULL;
	jogadorlist jogador;
	f = fopen(nomeficheiro, "rb");
	if (f == NULL) {
		// ficheiro n�o existe... siga
		return;
	}
	// L� jogadores
	while ((fread(&jogador, sizeof(jogadorlist), 1, f) == 1)) {
		novo = (pjogadorlist)malloc(sizeof(jogadorlist));
		*novo = jogador;
		// actualiza lastID
		if (jogador.id > jogadores->lastid) {
			jogadores->lastid = jogador.id;
		}
		novo->prox = NULL;
		jogadores->nJogadores++;
		if (jogadores->list == NULL) {
			jogadores->list = novo;
		}
		else {
			ultimo->prox = novo;
		}
		ultimo = novo;
		//actualiza lista do top 10
		jogadores_top(jogadores,novo);
	}
	fclose(f);
}

/* ---------------- Inserir Jogador ---------------- */
pjogadorlist jogadores_inserir(pjogadorlist p, pjogadorlist novo) {
	pjogadorlist aux;
	novo->prox = NULL;
	if (p == NULL) {
		p = novo;
	}
	else {
		aux = p;
		while (aux->prox != NULL)
			aux = aux->prox;
		aux->prox = novo;
	}
	return p;
}

/* ---------------- Inserir Jogador ---------------- */
void jogadores_novo(pjogadores_ctrl jogadores) {
	pjogadorlist novo;
	printHeader("Novo Jogador");
	novo = (pjogadorlist)malloc(sizeof(jogadorlist));
	if (novo == NULL) {
		printf("\n[ERRO] Acesso a memoria...");
		return;
	}
	readString("Nome:", novo->nome, NOME_JOGADOR_TAM, 0);
	if (strlen(novo->nome) == 0) {
		printf("Nome do jogador e' obrgatorio, operacao cancelada!\n");
		WaitEnterKey(1);
	}
	else {
		jogadores->lastid++;
		novo->id = jogadores->lastid;
		novo->j_ganhos = 0;
		novo->j_perdidos = 0;
		novo->prox = NULL;

		jogadores->list = jogadores_inserir(jogadores->list, novo);
	}
}

/* ---------------- Editar Jogador ---------------- */
void jogadores_editar(pjogadores_ctrl jogadores){
	pjogadorlist actual = jogadores->list; // Aponta para o incio de lista
	int id;
	if (actual == NULL) {
		printf("\nNao existe jogadores...\n\n");
		return;
	}
	else {
		printf("Indique o numero do Jogador que pretende editar.\n");
		id = readInteger("ID:", 1, jogadores->lastid);
		while (actual != NULL && (actual->id < id)) // enquanto forem diferentes
		{
			actual = actual->prox; // Avanca com o ponteiro para a proxima lista
		}
		if (actual != NULL && actual->id == id ) {
			printf("Alterar o nome %s para:\n",actual->nome);
			readString(">",actual->nome,NOME_JOGADOR_TAM,1);			
		}
		else {
			printf("Jogador %d nao encontrado\n", id);
			WaitEnterKey(1);
		}
	}

}

/* ---------------- Remover Jogador ---------------- */
void jogadores_remover(pjogadores_ctrl jogadores) {
	pjogadorlist anterior = NULL, actual = jogadores->list; // Aponta para o incio de lista
	int id;
	if (actual == NULL) {
		printf("\nNao existe jogadores...\n\n");
		return;
	}
	else {
		printf("Indique o numero do Jogador que pretende eliminar.\n");
		id = readInteger("ID:", 1, jogadores->lastid);
		while (actual != NULL && (actual->id < id)) // enquanto forem diferentes
		{
			anterior = actual;
			actual = actual->prox; // Avanca com o ponteiro para a proxima lista
		}
		if (actual != NULL && actual->id == id ) {
			if (anterior == NULL) // � o primeiro elemento
			{
				jogadores->list = actual->prox;
			}
			else {
				anterior->prox = actual->prox;
			}
			free(actual);
			jogadores->nJogadores--;
		}
		else {
			printf("Jogador %d nao encontrado\n", id);
			WaitEnterKey(1);			
		}
	}

}

pjogadorlist jogadores_encontra(pjogadores_ctrl lista, int id) {
	pjogadorlist actual = lista->list;
	if (id == 0)
		return NULL;
	while (actual != NULL && (actual->id < id)) // enquanto forem diferentes
	{
		actual = actual->prox; // Avanca com o ponteiro para a proxima lista
	}
	if (actual == NULL) {
		return NULL;
	}
	else {
		return actual;
	}
}

/*
 fun��o que mostra a lista de jogadores no ecran
 inputs:
 jctrl     - ponteiro para a estrutura que controla os jogadores
 filtro    - string para filtrar
 Pagina    - n� da pagina a listar
 tamPagina - N� de linhas da p�gina

 */
void jogadores_visualizar(pjogadores_ctrl jctrl, char *filtro, int Pagina, int tamPagina) {
	int count = 1;
	pjogadorlist lista = jctrl->list;
	if (lista == NULL)
		printf("\nNao existe jogadores...\n\n");
	else {
		printf("\n");
		if (strlen(filtro) > 0) {
			printf("Filtro: '%s'\n", filtro);
			strupr(filtro);
		}
		print_left("ID", 5, 0);
		print_left("Nome", 21, 0);
		print_left("Ganhos", 8, 0);
		print_left("Perdidos", 8, 0);
		printf("\n");
		printStrings("=", 5 + 21 + 8 + 8);
		printf("\n");
		// posionar na p�gina
		while (lista != NULL && count < Pagina * tamPagina) {
			if (strlen(filtro) == 0 || (searchStr(lista->nome, filtro) > 0)) {
				count++;
			}
			lista = lista->prox;
		}
		count = 1;
		while (lista != NULL && count < tamPagina) {
			if (strlen(filtro) == 0 || (searchStr(lista->nome, filtro) > 0)) {
				print_righti(lista->id, 4, 1);
				printf(" ");
				print_left(lista->nome, 20, 1);
				printf(" ");
				print_righti(lista->j_ganhos, 7, 1);
				printf(" ");
				print_righti(lista->j_perdidos, 7, 1);
				printf("\n");
				count++;
			}
			lista = lista->prox;
		}
		while (count < tamPagina) {
			printf("\n");
			count++;
		}
		printStrings("=", 5 + 21 + 8 + 8);
		printf("  Pagina %d/%d\n", Pagina + 1, (jctrl->nJogadores / tamPagina) + 1);
	}
}

/* ---------------- Main Menu ---------------- */
pjogadorlist jogadores_main(pjogadores_ctrl lista, int encontra) {
	int pagina = 0;
	int linhas = 16;
	char opcao[20];
	char fim = 'M';
	char filtro[NOME_JOGADOR_TAM] = "\0";
	do {
		if (encontra == 1) {
			printHeader("Pesquisa Jogadores");
		}
		else {
			printHeader("Lista Jogadores");
		}
		jogadores_visualizar(lista, filtro, pagina, linhas);
		if (encontra == 1) {
			readString("(V)oltar, (N)ovo, (R)emover, (E)editar, (F)iltro, (S)eguinte, (A)nterior, ID:", opcao, 20, 0);
			fim = 'V';
		}
		else {
			opcao[0] = readOpcao("(M)enu, (N)ovo, (R)emover, (E)editar, (F)iltro, (S)eguinte, (A)nterior:", "MNREFSA");
			opcao[1] = '\0';
			fim = 'M';
		}
		switch (toupper(*opcao)) {
		case 'N':
			jogadores_novo(lista);
			jogadores_top_reordena(lista);
			break;
		case 'R':
			jogadores_remover(lista);
			jogadores_top_reordena(lista);
			break;
		case 'E':
			jogadores_editar(lista);
			break;
		case 'F':
			readString("Filtro (vazio para cancelar):", filtro, NOME_JOGADOR_TAM, 0);
			pagina = 0;
			break;
		case 'S':
			if (pagina + 1 <= lista->nJogadores / linhas) {
				pagina++;
			}
			else {
				printf("Ultima pagina...");
				WaitEnterKey(1);
			}
			break;
		case 'A':
			if (pagina - 1 >= 0) {
				pagina--;
			}
			else {
				printf("Ultima pagina...");
				WaitEnterKey(1);
			}
			break;
		default:
			// verificar se foi id v�lido
			if (encontra == 1) {
				return jogadores_encontra(lista, atoi(opcao));
			}
			break;
		}
	}
	while (toupper(*opcao) != fim);
	return NULL;
}

void jogadores_top_reordena(pjogadores_ctrl ctrl)
{
	int i;
	pjogadorlist p;
	ctrl->nJogos=0;
	for(i=0;i<JOGODORES_TOP;i++) ctrl->top[i]=NULL;
	p=ctrl->list;
	while (p!=NULL) {
		jogadores_top(ctrl,p);
		p=p->prox;
	}
}

void jogadores_top(pjogadores_ctrl ctrl, pjogadorlist jogador)
{
	int i,j;
	ctrl->nJogos+=jogador->j_ganhos;
	if (jogador->j_ganhos==0) return;
	for(i=0;i<JOGODORES_TOP;i++)
	{

	   if (ctrl->top[i]==NULL || jogador->j_ganhos>ctrl->top[i]->j_ganhos) { 
		   //o jogador tem mais jogos ganhos que o proximo... vai ficar � frente e os outros chegam para traz
	      if (ctrl->top[i]!=NULL)
		  for(j=JOGODORES_TOP-1;j>=i;j--)
		  {
		     if (ctrl->top[j]!=NULL)
			   ctrl->top[j+1]=ctrl->top[j];
		  }
		  ctrl->top[i]=jogador;
		  return;
	   }
	}
}
