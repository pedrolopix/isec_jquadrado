#include "jogadores.h"
#define TAB_NLINHAS_MAX 8
#define TAB_NCOLUNAS_MAX 15
#define TAB_NLINHAS_MIN 2
#define TAB_NCOLUNAS_MIN 2
#define TAB_NLINHAS_JOGO_MAX 20
#define TAB_NCOLUNAS_JOGO_MAX 70
#define TAB_NLINHAS_DEF 4
#define TAB_NCOLUNAS_DEF 5
#define NJOGADORES_MAX 10
#define	NOME_FICHEIRO_TAM 40
#define NOME_JOGADOR_TAM 20
#define PALAVA_PASSE_TAM 5
#define JOGO_HEADER "JOGOV2"
#define JOGO_HEADER_SIZE 7


enum jogo_ret {
	ret_menu, ret_jogada_invalida, ret_continua, ret_perdeu, ret_ganhou, ret_proxima_jogada, ret_ja_jogado,
	ret_ja_fez_undo, ret_erro_memoria
};

typedef struct jogador_jogo_struct jogador_jogo, *pjogador_jogo;

typedef struct jogador_jogo_struct {
	int id;
	pjogadorlist jogador; // ponteiro para a estrutura do jogador_jogo.
	char nome[NOME_JOGADOR_TAM];
	int perdeu;
	int undo;
};

typedef struct {
	int *tabuleiro; // Array com o tabuleiro
	jogador_jogo jogadores[NJOGADORES_MAX];
	int nJogador; // jogador_jogo a jogar...
	int nJogadorAnterior; // jogador anterior...
	int nJogadores;
	int nJogadoresEmJogo;
	int nJogadas;
	int linhas;
	int colunas;
	int *pecasJogadas; // array dinamico para as pe�as jogadas
	char palavra_passe[PALAVA_PASSE_TAM];
	enum jogo_ret ultimoStat;
} jogo, *pjogo;

enum jogo_ret DoJogoda(pjogo jogo, char *aop);
enum jogo_ret jogo_main(pjogo jogo, char *aop);
enum jogo_ret jogo_menu(pjogo jogo, pjogadores_ctrl jctrl);
int jogo_aumenta_tabuleiro(pjogo jogo);
int jogo_coor2index(pjogo jogo, char *op);
int jogo_ler(pjogadores_ctrl jctrl, pjogo jogo, char *nomeficheiro, char *palavrapasse);
int jogo_peca(pjogo jogo, int linha, int coluna);
int jogo_pesquisa_jogador(pjogo jogo, char *nome);
int jogo_verifica_jogada(pjogo jogo, int idx);
void jogo_adiciona_jogador(pjogo jogo, pjogadorlist jogador);
void jogo_adiciona_jogadorAnonimo(pjogo jogo, char *nome);
void jogo_continua(pjogo jogo, pjogadores_ctrl jctrl);
void jogo_finaliza(pjogo jogo);
void jogo_inicia(pjogo jogo, int linhas, int colunas);
void jogo_inicia_variaveis(pjogo jogo);
void jogo_mostra(pjogo jogo);
void jogo_mostra_tabuleiro(pjogo jogo);
void jogo_proximo_jogador(pjogo jogo);
void jogo_regista_jogada(pjogo jogo, int idx);
void jogo_reinica_tabuleiro(pjogo jogo, int linhas, int colunas);
void jogo_remove_jogador(pjogo jogo, int id);
